-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 07, 2023 at 04:51 PM
-- Server version: 8.0.30
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `medweb`
--

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int NOT NULL,
  `uuid` varchar(36) NOT NULL,
  `name` varchar(128) NOT NULL,
  `heading` varchar(255) DEFAULT NULL,
  `paragraph` varchar(255) DEFAULT NULL,
  `src` varchar(128) DEFAULT NULL,
  `alt` varchar(128) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `status_change_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(128) DEFAULT NULL,
  `updated_by` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `uuid`, `name`, `heading`, `paragraph`, `src`, `alt`, `is_active`, `status_change_at`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(5, '6480397d845ae', 'Slider-1', 'Ensure Your Medical Services', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus, unde.', '6480397d808f3_s1.jpg', 'image-1', 1, NULL, '2023-06-07 08:02:05', '2023-06-07 08:02:05', 'labib', 'labib'),
(6, '648039acf3d5f', 'Slider-2', 'Ensure Your Medical Services', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus, unde.', '648039aced7d8_s2.jpg', 'image-2', 1, NULL, '2023-06-07 08:02:52', '2023-06-07 08:02:52', 'labib', 'labib'),
(7, '648039e1ed0bf', 'Slider-3', 'Ensure Your Medical Services', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus, unde.', '648039e1ec8ed_s3.jpg', 'image-3', 1, '2023-06-07 16:47:12', '2023-06-07 08:03:45', '2023-06-07 08:03:45', 'labib', 'labib'),
(8, '64803cf03b7d7', 'Slider-4', 'Ensure Your Medical Services', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus, unde.', '64806ec3f060c_s4.jpg', 'image', 0, '2023-06-07 16:49:20', '2023-06-07 08:16:48', '2023-06-07 16:48:18', 'labib2', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
