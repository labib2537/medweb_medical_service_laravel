<?php
$path = $_SERVER['DOCUMENT_ROOT'].'/medweb_medical_service';
include_once($path.'/config.php');



  use \MedWeb\Slider;
  use \MedWeb\utility\Validator;
  use \MedWeb\utility\Utility;

  $id = Utility::sanitize($_POST['id']);
  $state = Utility::sanitize($_POST['status']);
   if(!Validator::empty($id)){
	 $slider = new Slider();
	 $result = $slider->updateStatus($id, $state);
     }else{
	 dd("ID can't be empty!"); //using session
     }
           
    if($result){
        $message = "Slider Status is updated Successfully";
        //$_SESSION['message'] = $message;
        set_session('message', $message);
        redirect('slider-list.php');
    }
  
                
?>            