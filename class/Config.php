<?php

namespace MedWeb;

class Config{


    static public $driver = 'mysql';

    // define can't be use in class
    // constant only can be use in class 
    const DB_HOST = 'localhost';  
    const DB_USER = 'root';
    const DB_PASS = '';
    const DB_NAME = 'medweb';



    static public function jsonData()
    {
        return self::docroot().'/JsonData'.'/';
    }
   
    static public function docroot(){
        return $_SERVER['DOCUMENT_ROOT'].'/medweb_medical_service';
    }


}