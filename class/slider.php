<?php

namespace MedWeb;

use MedWeb\Config;

class Slider{
    public $id = null;
    public $name = null;
    public $heading = null;
    public $paragraph = null;
    public $src = null;
    public $alt = null;

    private $json = null;

    private $connect = null;

    public function __construct(){
         if(Config::$driver == 'mysql'){
            $this->connectDB();
         }elseif(Config::$driver == 'json'){
            $this->jsonConnect();
         }

    }

	public function list()
    {
        $pdo_statement = $this->connect->prepare("SELECT * FROM `sliders`");
	    $pdo_statement->execute();
	    $sliders = $pdo_statement->fetchAll(\PDO::FETCH_OBJ);
        return $this->data = $sliders;
    }

    public function create()
    {
        
    }

    public function store($slider)
    {
        //prepare the sql
        $sql = $this->connect->prepare('INSERT INTO `sliders` (`uuid`, `name`, `heading`, `paragraph`, `src`, `alt`, `created_at`, `updated_at`, `created_by`, `updated_by`) 
               VALUES (:uuid, :name, :heading, :paragraph, :src, :alt, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, :created_by, :updated_by)');

            $sql->bindParam(':uuid', $slider->uuid, \PDO::PARAM_STR);
            $sql->bindParam(':name', $slider->name, \PDO::PARAM_STR);
            $sql->bindParam(':heading', $slider->heading, \PDO::PARAM_STR);
            $sql->bindParam(':paragraph', $slider->paragraph, \PDO::PARAM_STR);
            $sql->bindParam(':src', $slider->src, \PDO::PARAM_STR);
            $sql->bindParam(':alt', $slider->alt, \PDO::PARAM_STR);
            $sql->bindParam(':created_by', $slider->created_by, \PDO::PARAM_STR);
            $sql->bindParam(':updated_by', $slider->updated_by, \PDO::PARAM_STR);
           
            try{
	          $sql->execute();
              return true;
            }catch(\Exception $e){
              echo $e->getMessage();
              return false;
            }
        
    }

    public function show($id)
    {
        $pdo_statement = $this->connect->prepare("SELECT * FROM `sliders` WHERE id=:id");
        $pdo_statement->bindParam(':id', $id, \PDO::PARAM_INT);
	    $pdo_statement->execute();
	    $pdo_statement->setFetchMode(\PDO::FETCH_OBJ);
        return $slider = $pdo_statement->fetch();
        
    }

    public function edit($id)
    {

    //    return $this->find($id);
    $pdo_statement = $this->connect->prepare("SELECT * FROM `sliders` WHERE id=:id");
    $pdo_statement->bindParam(':id', $id, \PDO::PARAM_INT);
    $pdo_statement->execute();
    $pdo_statement->setFetchMode(\PDO::FETCH_OBJ);
    return $slider = $pdo_statement->fetch();

    }

    public function update($slider)
    {
    
        $sql = $this->connect->prepare("UPDATE `sliders` SET `name` = '$slider->name', `heading` = '$slider->heading',
        `paragraph` = '$slider->paragraph', `src` = '$slider->src', `alt` = '$slider->alt', `updated_at` = CURRENT_TIMESTAMP, 
        `updated_by` = '$slider->updated_by' WHERE `sliders`.`id` = '$slider->id'");
       

        // $sql = $this->connect->prepare("UPDATE `sliders` SET name='" . $slider->name . "', heading = '" . $slider->heading . "', paragraph = '" . $slider->paragraph . "',
        // src = '" . $slider->src . "', alt = '" . $slider->alt . "', updated_by='" . $slider->updated_by . "' WHERE id = " . $slider->id);

            // $sql->bindParam(':name', $slider->name, \PDO::PARAM_STR);
            // $sql->bindParam(':heading', $slider->heading, \PDO::PARAM_STR);
            // $sql->bindParam(':paragraph', $slider->paragraph, \PDO::PARAM_STR);
            // $sql->bindParam(':src', $slider->src, \PDO::PARAM_STR);
            // $sql->bindParam(':alt', $slider->alt, \PDO::PARAM_STR);
            // $sql->bindParam(':updated_by', $slider->updated_by, \PDO::PARAM_STR);
 
            try{
	          $sql->execute();
              return true;
            }catch(\Exception $e){
              echo $e->getMessage();
              return false;
            }
    }

    public function updateStatus($id, $state) //completely delete
    {
        if(empty($id)){
            return;
        }
        // dd($state);
        if($state==1)
        {
            $sql = $this->connect->prepare("UPDATE `sliders` SET `is_active` = '0', `status_change_at` = CURRENT_TIMESTAMP WHERE `sliders`.`id` = :id");
        }else{
            $sql = $this->connect->prepare("UPDATE `sliders` SET `is_active` = '1', `status_change_at` = CURRENT_TIMESTAMP WHERE `sliders`.`id` = :id");
        }
        $sql->bindParam(':id', $id, \PDO::PARAM_INT);
        return $sql->execute();
       
    
    }



    public function destroy($id) //completely delete
    {
        if(empty($id)){
            return;
        }
        
        $pdo_statement = $this->connect->prepare("DELETE FROM `sliders` WHERE id=:id");
        $pdo_statement->bindParam(':id', $id, \PDO::PARAM_INT);
        return $pdo_statement->execute();
    
    }

    public function trash()
    {
        
    }

    public function delete() //soft delete
    {
        
    }

    private function prepare($slider)
    {
        $slider->id = uniqid();
        return $slider;
    }


    private function jsonWrite(){
        $jsonfile = Config::jsonData()."slide.json";
        if(file_exists($jsonfile)){
            $result = file_put_contents($jsonfile, json_encode($this->json));
            return true;
        }
        else{
          echo "Not Found!";
          return false;
        }
    }

    public function find($id=null)
    {
        if(empty($id) || is_null($id)){
            return false;
        }
        foreach($this->json as $key=>$slide){
            if($slide->id==$id) {
                break;
            }
        }
        return $slide;
        
    }

    private function connectDB()
    {
       //connect to DB

       

        try{
	       $this->connect = new \PDO('mysql:host='.Config::DB_HOST.';dbname='.Config::DB_NAME.';charset=utf8', Config::DB_USER, Config::DB_PASS);
        }catch(\PDOException $e){
            echo $e->getMessage();
        } 
    }

    private function jsonConnect(){
        $fileData = file_get_contents(Config::jsonData()."slide.json");
        $this->json = json_decode($fileData);
    }
    
}